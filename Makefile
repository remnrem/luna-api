include Makefile.inc 

OUTPUT = example

LIB = -lluna -lfftw3

SRC = example.cpp

HDR = 

OBJ = example.o

all : $(OUTPUT)

$(OUTPUT) :
	$(CXX) -o $(OUTPUT) $(OBJ) $(LDFLAGS) $(LIB)

ifeq ($(ARCH),MAC)
	install_name_tool -change libluna.dylib $(LUNA_BASE)/libluna.dylib $(OUTPUT)
endif


$(OBJ) : $(HDR)

.cpp.o : 
	$(CXX) $(CXXFLAGS) -c $*.cpp

.SUFFIXES : .cpp .o $(SUFFIXES)

$(OUTPUT) : $(OBJ) 

FORCE:

clean:
	rm -f *.o *~ ${OUTPUT}
