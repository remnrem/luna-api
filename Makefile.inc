
##
## Target architecture (MAC or LINUX)
##

ARCH = LINUX

##
## path to luna base
##

LUNA_BASE = ${HOME}/luna-base/

##
## luna dependencies (as absolute paths), if they are not accessible system-wide
## (nb, for Mac, /usr/local has to be added explicitly)
##

FFTW = /usr/local/

LUNA_DEP = ${FFTW}


##
## main flags  
##


CXX = g++

CXXFLAGS = -O2 -std=c++0x -I. -I${LUNA_BASE} -I${LUNA_DEP}/include -DNO_HPDFLIB

LDFLAGS = -L. -L${LUNA_BASE} -L${LUNA_DEP}/lib
