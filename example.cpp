#include "luna.h"

extern globals global;

extern writer_t writer;

extern logger_t logger;

void get_annotations( edf_t & edf );

uint64_t update_size( edf_t & edf );

bool add_annotations( edf_t & edf , const std::string & annotfile ); 


int main(int argc , char ** argv ) 
{

  // required set-up
  global.init_defs();

  // turns off console log
  global.api(); 
  // globals::silent = false;
  
  std::cout << "xluna " << globals::version << " " << globals::date << "\n";

  std::string edf_file = "/Users/smp37/dropbox/fun/my-sleep/Purcell06072016.edf";
  
  edf_t edf; 
  
  bool okay = edf.attach( edf_file , "smp" );

  if ( ! okay ) Helper::halt( "failed to attach file" );

  get_annotations( edf );

  std::exit(0);
  
  // use a retval_t mechanism to catch results
  retval_t ret;

  writer.use_retval( &ret );

  // evaluate a command
  cmd_t cmd( "EPOCH & PSD epoch" );

  okay = cmd.eval( edf ); 

  if ( ! okay ) Helper::halt( "failed to run command" ) ;
  
  // look at output (dumps to std::cout)
  ret.dump();
  
  std::exit(0);

}



void get_annotations( edf_t & edf )
{

  // example of attaching annotations from various sources
  // and setting and fetching masks based on those annotations
  
  // XML files
  // FTR files
  // annot files
  // epoch masks
  // epoch annotations

  std::string XML = "/home/shaun/Dropbox/my-sleep/SP_20160607_NSRR.xml";

  // id_{id}_feature_{name}.ftr
  std::string FTR1 = "/home/shaun/Dropbox/my-sleep/annots/id_smp_feature_SPINDLES-11-EEG3.ftr";
  std::string FTR2 = "/home/shaun/Dropbox/my-sleep/annots/id_smp_feature_SPINDLES-15-EEG3.ftr";

  // interval-based annotations
  std::string IANNOT = "/home/shaun/Dropbox/my-sleep/spindles1-smp.annot";
  
  // full-format epoch-based annotations
  std::string EANNOT = "/home/shaun/Dropbox/my-sleep/epoch1.annot";
  
  // simple file format (
  // awk ' BEGIN { for (i=1;i<=1022;i++) printf i % 2"\n" } ' > misc.epoch.mask
  std::string EPOCH_MASK = "/home/shaun/Dropbox/my-sleep/misc.epoch.mask";

  // awk ' BEGIN { for (i=1;i<=1022;i++) { if ( i % 10 ) print "T"; else print "A" } } ' > misc.epoch.annot
  std::string EPOCH_ANNOT = "/home/shaun/Dropbox/my-sleep/misc.epoch.annot";


  //
  // fetch annots
  //

  bool okay = false;
  
  okay = add_annotations( edf , XML );
  std::cout << "XML fetch play\n";
  
  okay = add_annotations( edf , FTR1 );
  std::cout << "FTR1 fetch play\n";
  
  okay = add_annotations( edf , FTR2 );
  std::cout << "FTR2 fetch play\n";
  
  okay = add_annotations( edf , IANNOT );
  std::cout << "IANNOT fetch play\n";
  
  //  okay = add_annotations( edf , EANNOT );
  //std::cout << "EANNOT fetch play\n";
  
  // okay = add_annotations( edf , EPOCH_MASK );
  // std::cout << "EPOCH_MASK fetch play\n";
  
  // okay = add_annotations( edf , EPOCH_ANNOT );
  // std::cout << "EPOCH ANNOT fetch play\n";  

  std::cout << "about to update total size...\n";
  uint64_t total_size = update_size( edf );
  std::cout << "tot size = " << total_size << "\n";
  
  //
  // use annots
  //
  
  // get names of all annotations
  std::vector<std::string> annot_labels = edf.timeline.annotations.names();

  std::cout << "found annot_labels " << annot_labels.size() << "\n";
  
  // find an annotation based on its name
  if ( 0 ) { 
    std::string label = "a1";
    annot_t * a = edf.timeline.annotations.find( label );
    if ( a != NULL ) { // ...
    }
  }  


  
  // these two accumulate all annotations
  // not clear why two sets are needed...

//   std::map<std::string,interval_evt_map_t> allannots;
//   std::set<feature_t> features; // interval + labels  
    
//   std::vector<std::string>::const_iterator aa =  annot_labels.begin();
//   while ( aa != annot_labels.end() )
//     {
//       std::cout << *aa << "\n";
      
//       annot_t * a = edf.timeline.annotations.find( *aa );

//       if ( a == NULL ) { ++aa; continue; }       
      
//       // get all specific annots, given annot_t * a 

        
//       std::string aclass = a->name;

//       // total_size is updated as annotations are loaded in, i.e.
//       // in case some are larger than the EDF signal
  
//       allannots[ aclass ] = a->extract( interval_t( 0 , total_size ) );

//       // for these annotation, get the specific list of events
  
//       interval_evt_map_t & tmp = allannots[ a->name ];
      
//       interval_evt_map_t::const_iterator ii = tmp.begin();
//       while ( ii != tmp.end() )
// 	{
	  
// 	  feature_t feature;
	  
// 	  feature.feature = ii->first;
// 	  feature.window = ii->first;
	  
// 	  // get event(s)
	  
// 	  const std::vector<const event_t*> & events = ii->second;
// 	  std::string label = "";
// 	  for (int e=0;e<events.size();e++)
// 	    {
// 	      if ( e!=0 ) label += ",";
// 	      label += events[e]->label;
// 	      feature.add_data( events[e]->metadata );
// 	    }
	  
	  
// 	  if ( label != aclass )
// 	    feature.signal = label;
	  
// 	  feature.label = aclass;
	  
// 	  // display
	  
// 	  std::cout << "\n" << feature.as_string( "\t" ) << "\n";
// 	  std::cout << feature.print_data( )  << "\n";
	  
// 	  // store                                                                                                                         
// 	  features.insert( feature );
	  
// 	  // next interval                                                                                                                 
// 	  ++ii;
// 	}

      
//       // next annotation
//       ++aa;
//     }



//   //
//   // List all features
//   //

//   std::set<feature_t>::const_iterator ff = features.begin();
//   while ( ff != features.end() )
//     {
//       std::cout << ff->as_string( " # " ) << "\n";
//       //std::cout << feature.print_data( )  << "\n";
//       ++ff;
//     }
  
}




//
// Attach annotations
//

bool add_annotations( edf_t & edf , const std::string & annotfile )
{
  return false;

  // Load fully any XML annotations; register names for any non-XML files
  // in the EDF's flist
  
  //  bool loaded = edf.populate_alist( annotfile ) ;

  //  std::cout << " loaded " << annotfile << " --> " << loaded << "\n";
  // problem?
  
  //  if ( ! loaded ) return false;

  // Do not process XML or FTR files, these have already been added
  // by the populate_alist()

//   if ( Helper::file_extension( annotfile , "xml" ) ) return true;
//   if ( Helper::file_extension( annotfile , "ftr" ) ) return true;

//   // Get annot name in this file
//   if ( edf.flist.find( annotfile ) == edf.flist.end() ) 
//     Helper::halt( "problem finding " + annotfile );
  
//   const std::string & annot_name = edf.flist[ annotfile ];
  
//   // Connect to the timeline
//   annot_t * myannot = edf.timeline.annotations.add( annot_name );
      
//   loaded = myannot->load( annotfile );
  
//   return loaded; 
        
}

uint64_t update_size( edf_t & edf )
{

  // for all annotations
  
  uint64_t total_size = edf.timeline.last_time_point_tp + 1;
  
  std::map<std::string,annot_t*>::const_iterator ai = edf.timeline.annotations.annots.begin();
  while ( ai != edf.timeline.annotations.annots.end() )
    {

      annot_t * annot = ai->second;

      uint64_t max_tp = annot->maximum_tp();
      
      //      std::cout << "max t = " << ai->first << " --> " << max_tp << "\n";
      
      if ( max_tp >= total_size )
        {
          total_size = max_tp + 1;
          //std::cout << "adjusting total size to " << total_size << "\n";
        }
      ++ai;
    }

  return total_size;
}







